#include <fstream>
#include <iostream>
#include <string>
#include "DroneAmbassador.hpp"
#include "fedtime.hh"
#include <sys/time.h>
#include <cstring>
#include "utils.hpp"

using namespace std;

DroneAmbassador::DroneAmbassador(const string &federateName, Drone &local){
  this->local = &local;
  this->federateName = federateName;
  // initialize all the variable values
  this->federateTime = 0.0;
  this->federateLookahead = 1.0;
  this->isRegulating = false;
  this->isConstrained = false;
  this->isAdvancing = false;
  this->isAnnounced = false;
  this->isReadyToRun = false;

}


DroneAmbassador::~DroneAmbassador()
throw( RTI::FederateInternalError )
{
}


///////////////////////////////////////////////////////////////////////////////
///////////////////////// Object Management Callbacks /////////////////////////
///////////////////////////////////////////////////////////////////////////////


void
DroneAmbassador::discoverObjectInstance(
    RTI::ObjectHandle theObject,
    RTI::ObjectClassHandle theObjectClass,
    const char *theObjectName)
throw(RTI::CouldNotDiscover,
RTI::ObjectClassNotKnown,
RTI::FederateInternalError) {
  cout << "Drone " << this->local->id << ": "
       << "Discoverd Object: handle=" << theObject
       << ", classHandle=" << theObjectClass
       << ", name=" << theObjectName << endl;
}


void
DroneAmbassador::reflectAttributeValues(
    RTI::ObjectHandle theObject,
    const RTI::AttributeHandleValuePairSet &theAttributes,
    const RTI::FedTime &theTime,
    const char *theTag,
    RTI::EventRetractionHandle theHandle)
throw(RTI::ObjectNotKnown,
RTI::AttributeNotKnown,
RTI::FederateOwnsAttributes,
RTI::InvalidFederationTime,
RTI::FederateInternalError) {
  cout << "Drone " << this->local->id
       << ": Reflection Received. "
       << "Object: " << theObject << endl;

  Drone drone = Drone(theObject);

  for (RTI::ULong i = 0; i < theAttributes.size(); i++) {
    RTI::ULong length = theAttributes.getValueLength(i);
    char *value = theAttributes.getValuePointer(i, length);
    std::vector<std::string> result = vector<string>();
    split(value, ':', result);

    if ( result.at(0) == "posX") {
      drone.setPosX(stol(result.at(1)));
    }
    else if ( result.at(0) == "posY") {
      drone.setPosY(stol(result.at(1)));
    }
    else if ( result.at(0) == "teta") {
      drone.setTeta(stol(result.at(1)));
    }
    else if ( result.at(0) == "speed") {
      drone.setSpeed(stol(result.at(1)));
    }
  }
  auto it = remote.find(theObject);
  remote.insert(it, pair<RTI::ObjectHandle, Drone>(theObject, drone));
  this->hasData = true;
}

void DroneAmbassador::calculateDistance() {
  hasColision = false;
  for (std::pair<RTI::ObjectHandle, Drone> element : this->remote) {
    RTI::ObjectHandle id = element.first;
    Drone other = element.second;
    long distance = this->local->distance(other);
    cout << "Drone " << this->local->id << ": "
         << "Distance to " << id << ": " << distance << endl;
    if (distance <= this->local->proximity_limit) {
      hasColision = true;
      cout << "Drone " << this->local->id
           << ": Colision imminent with drone " << id
           << ". Distance is :" << distance << endl;
    }
  }
}

void
DroneAmbassador::receiveInteraction(
    RTI::InteractionClassHandle theInteraction,
    const RTI::ParameterHandleValuePairSet &theParameters,
    const RTI::FedTime &theTime,
    const char *theTag,
    RTI::EventRetractionHandle theHandle)
throw(RTI::InteractionClassNotKnown,
RTI::InteractionParameterNotKnown,
RTI::InvalidFederationTime,
RTI::FederateInternalError) {
  cout << "Interaction Received:";

  // print the handle
  cout << " handle=" << theInteraction;
  // print the tag
  cout << ", tag=" << theTag;
  // print the time
  cout << ", time=" << convertTime(theTime);

  // print the attribute information
  cout << ", parameterCount=" << theParameters.size() << endl;
  for (RTI::ULong i = 0; i < theParameters.size(); i++) {
    // print the parameter handle
    cout << "\tparamHandle=" << theParameters.getHandle(i);
    // print the parameter value
    RTI::ULong length = theParameters.getValueLength(i);
    char *value = theParameters.getValuePointer(i, length);

    cout << ", paramValue=" << value << endl;\
  }
}


void
DroneAmbassador::removeObjectInstance(RTI::ObjectHandle theObject, const char *theTag)
throw(RTI::ObjectNotKnown, RTI::FederateInternalError) {
  cout << "Object Removed: handle=" << theObject << endl;
}


void
DroneAmbassador::removeObjectInstance(
    RTI::ObjectHandle theObject,
    const RTI::FedTime &theTime,
    const char *theTag,
    RTI::EventRetractionHandle theHandle)
throw(RTI::ObjectNotKnown,
RTI::InvalidFederationTime,
RTI::FederateInternalError) {
  cout << "Object Removed: handle=" << theObject << endl;
}


void DroneAmbassador::turnUpdatesOnForObjectInstance(
    RTI::ObjectHandle theObject, const RTI::AttributeHandleSet &set)
  throw (RTI::ObjectNotKnown, RTI::AttributeNotOwned, RTI::FederateInternalError){
  cout << "Drone " << theObject << ": Update attributes turned on" << endl;
}


///////////////////////////////////////////////////////////////////////////////
/////////////////////// Synchronization Point Callbacks ///////////////////////
///////////////////////////////////////////////////////////////////////////////
void
DroneAmbassador::synchronizationPointRegistrationSucceeded(const char *label)
throw(RTI::FederateInternalError) {
  cout << "Successfully registered sync point: " << label << endl;
}

void
DroneAmbassador::synchronizationPointRegistrationFailed(const char *label)
throw(RTI::FederateInternalError) {
  cout << "Failed to register sync point: " << label << endl;
}

void
DroneAmbassador::announceSynchronizationPoint(const char *label, const char *tag)
throw(RTI::FederateInternalError) {
  cout << "Synchronization point announced: " << label << endl;
  if (strcmp(label, "ReadyToRun") == 0)
    this->isAnnounced = true;
}

void
DroneAmbassador::federationSynchronized(const char *label)
throw(RTI::FederateInternalError) {
  cout << "Federation Synchronized: " << label << endl;
  if (strcmp(label, "ReadyToRun") == 0)
    this->isReadyToRun = true;
}

///////////////////////////////////////////////////////////////////////////////
//////////////////////////////// Time Callbacks ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////
void
DroneAmbassador::timeRegulationEnabled(const RTI::FedTime &theFederateTime)
throw(RTI::InvalidFederationTime,
RTI::EnableTimeRegulationWasNotPending,
RTI::FederateInternalError) {
  this->isRegulating = true;
  this->federateTime = convertTime(theFederateTime);
}

void
DroneAmbassador::timeConstrainedEnabled(const RTI::FedTime &theFederateTime)
throw(RTI::InvalidFederationTime,
RTI::EnableTimeConstrainedWasNotPending,
RTI::FederateInternalError) {
  this->isConstrained = true;
  this->federateTime = convertTime(theFederateTime);
}

void
DroneAmbassador::timeAdvanceGrant(const RTI::FedTime &theTime)
throw(RTI::InvalidFederationTime,
RTI::TimeAdvanceWasNotInProgress,
RTI::FederateInternalError) {
  this->isAdvancing = false;
  this->federateTime = convertTime(theTime);
}


double
DroneAmbassador::convertTime(const RTI::FedTime &theTime) {
  RTIfedTime castedTime = (RTIfedTime) theTime;
  return castedTime.getTime();
}
