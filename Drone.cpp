#include <cstring>
#include <cmath>
#include "RTI.hh"
#include "Drone.hpp"
#include "utils.hpp"

Drone::Drone(RTI::ObjectHandle id)
{
  // hla object handle
  this->id = id;
  // longitude
  this->posX = static_cast<double>(randint(-180, 180));
  // latitude
  this->posY = static_cast<double>(randint(-90, 90));
  // angle of direction (in degrees)
  this->teta = randint(0, 360);
  // actual speed of drone (in meters per second)
  this->speed = 1;
  // proximity limit to another drone (in meters)
  this->proximity_limit = 10;
}

double Drone::getPosX() {
  return this->posX;
}

double Drone::getPosY() {
  return this->posY;
}

int Drone::getSpeed() {
  return this->speed;
}

int Drone::getTeta() {
  return this->teta;
}

void Drone::setPosX(double pos) {
  this->posX = pos;
}

void Drone::setPosY(double pos) {
  this->posY = pos;
}

void Drone::setSpeed(int value) {
  this->speed = value;
}

void Drone::setTeta(int degrees) {
  this->teta = degrees;
}

double Drone::distance(Drone other) {
  return sqrt(pow(other.getPosX() - getPosX(), 2) + pow(other.getPosY() - getPosY(), 2));
}

void Drone::move(int time) {
  double distance(getSpeed() * time);
  setPosX(getPosX() + cos(radians(getTeta())) * distance);
  setPosY(getPosY() + sin(radians(getTeta())) * distance);
}
