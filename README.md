# Drones 

Simulation of drone swarm with HLA/RTI.

## Getting started

- Documentação do PyHLA
  - É útil para analisar como são realizadas as sequências das mensagens para iniciar ou terminar a comunicação entre o federado e a federação. [Veja mais detalhes aqui](http://www.nongnu.org/certi/PyHLA/manual/node10.html)
- Projeto Portico
  - [Implementação HLA em C++ e Java possui exemplos práticos e fáceis de implementar](https://github.com/openlvc/portico/tree/master/codebase/src/cpp/hla13/example)
- Tutorial em linguagem simples e clara sobre o HLA
  - [Pitch HLA Tutorial](http://pitchtechnologies.com/wp-content/uploads/2014/04/TheHLAtutorial.pdf)
 
  
## Installing

1. Antes de tudo, instale o CERTI. A pasta onde o código fonte do CERTI está chamaremos de `$CERTI_SOURCE`
2. Agora descompacte o arquivo deste projeto ou faça um clone do git.
```bash
$ tar zxvf meuExemplo.tar.gz
# ou
$ git clone git@gitlab.com:valbertovc/drones.git
```
3. Copie o conteudo desta pasta para o diretório "test" do CERTI

```bash
cp -r drones $CERTI_SOURCE/test
```

4) Dentro do diretório "test" edite o arquivo CMakeLists.txt para que ele fique assim:

```bash
$ gedit $CERTI_SOURCE/test/CMakeLists.txt 
```

```cmake
...
add_subdirectory(Billard)
...
add_subdirectory(testFederate)
add_subdirectory(drones)  # <-- somente esta linha será acrescentada

```

>  Isto fará com que o projeto seja compilado junto com os outros exemplos do CERTI

5. Para compilar entre no diretório "test" da pasta onde o certi foi compilado (normalmente chamada de `build` que fica dentro de `$CERTI_SOURCE`) e digite: 

```bash
$ cd $CERTI_SOURCE/build/test && make
```

6. Quando realizar qualquer mudanças no código você precisa recompilar. Para isso você pode recompilar todos os exemplos do certi ou apenas um específico.

```bash
# compila todos
$ cd $CERTI_SOURCE/build/test && make clean && make
# compila apenas "drones"
$ cd $CERTI_SOURCE/build/test/drones && make clean && make
```

7. Pronto. O exemplo está pronto para executar.


## Usage


Para executar, abra 2 terminais. Em um deles inicie o RTIG (rtig) e no outro faça:

```bash
$ rtig     # no terminal 1
$ ./drones # no terminal 2
```

Aperte ENTER em cada terminal para libera-los. Ao final da execução você verá o relatório de todos os dados produzidos durante a simulação.


A imagem abaixo mostra a comunicação executada nos comandos acima

## How to solve possible problems

### Cannot connect port / Connection refused

```bash
~/workspace/certi/build/test/Drones ./drones 
RTIA:: RTIA has thrown NetworkError exception.
RTIA:: Reason: Cannot connect port <60400> on addr <127.0.0.1> : error =Connection refused
RTIA:: End execution.
UN Socket(RecevoirUN) : : Connection reset by peer
libRTI: exception: NetworkError (read)
terminate called after throwing an instance of 'RTI::RTIinternalError'
Aborted (core dumped)
```

O erro ocorreu porque:
- Você tentou iniciar uma simulação e o `rtig` (Run-time infrastructure gateway) não está em execução.
- Verifique se o serviço `rtig` pode ser iniciado a partir do terminal.
- Em uma nova aba do terminal inicie o `rtig` e reexecute o comando de inicialização da simulação.


### RTI::CouldNotOpenFED

```bash
~/workspace/certi/build/test/Drones$ ./drones 
terminate called after throwing an instance of 'RTI::CouldNotOpenFED'
RTIA:: RTIA has thrown NetworkError exception.
RTIA:: Reason: Connection closed by client.

RTIA: Statistics (processed messages)
List of federate initiated services 
--------------------------------------------------
       1 Message::OPEN_CONNEXION (MSG#1)

List of RTI initiated services 
--------------------------------------------------

 Number of Federate messages : 1
 Number of RTIG messages : 0
Aborted (core dumped)
 TCP Socket  3 : total =        93 Bytes sent 
 TCP Socket  3 : total =        51 Bytes received
 UDP Socket  5 : total =         0 Bytes sent 
 UDP Socket  5 : total =         0 Bytes received
```

O erro ocorreu porque:
- O rtia (instância de RTIAmbassador) tentou iniciar uma federação fornecendo um arquivo `.fed` cujo nome está escrito errado ou não existe nas pastas de localização padrão do `trig`.
- Leia com atenção [esta página](http://www.nongnu.org/certi/certi_doc/Install/html/group__certi__FOM__FileSearch.html)
- Copie o arquivo para a pasta sugerida `$CERTI_HOME/share/federations` ou defina o local do arquivo na variável de ambiente `$CERTI_HOME/share/federations/`


### RTI::ErrorReadingFED

```bash
~/workspace/certi/build/test/Drones$ ./drones 
terminate called after throwing an instance of 'RTI::ErrorReadingFED'
Aborted (core dumped)
RTIA:: RTIA has thrown NetworkError exception.
RTIA:: Reason: Connection closed by client.

RTIA: Statistics (processed messages)
List of federate initiated services 
--------------------------------------------------
       1 Message::OPEN_CONNEXION (MSG#1)

List of RTI initiated services 
--------------------------------------------------

 Number of Federate messages : 1
 Number of RTIG messages : 0
 TCP Socket  3 : total =        94 Bytes sent 
 TCP Socket  3 : total =        69 Bytes received
 UDP Socket  5 : total =         0 Bytes sent 
 UDP Socket  5 : total =         0 Bytes received
```

O erro ocorreu porque:
- Existe um erro de escrita dentro do arquivo `.fed` fornecido.


### RTI::FederateNotExecutionMember

```bash
~/workspace/certi/build/test/Drones$ ./drones ex1
Didnt create federation, it already existed
Joined Federation as ex1
Synchronization point announced: ReadyToRun
 >>>>>>>>>> Press Enter to Continue <<<<<<<<<<

Achieved sync point: ReadyToRun, waiting for federation...
Successfully registered sync point: ReadyToRun
Federation Synchronized: ReadyToRun
Time Policy Enabled
 >>>>>>>>>> Atributo criado <<<<<<<<<<
 >>>>>>>>>> Atributos adicionados <<<<<<<<<<
terminate called after throwing an instance of 'RTI::FederateNotExecutionMember'
Aborted (core dumped)
RTIA:: RTIA has thrown NetworkError exception.
RTIA:: Reason: Connection closed by client.

RTIA: Statistics (processed messages)
List of federate initiated services 
--------------------------------------------------
       1 Message::OPEN_CONNEXION (MSG#1)
       1 Message::REGISTER_FEDERATION_SYNCHRONIZATION_POINT (MSG#7)
       1 Message::SYNCHRONIZATION_POINT_ACHIEVED (MSG#11)
       1 Message::PUBLISH_OBJECT_CLASS (MSG#29)
       1 Message::ENABLE_TIME_REGULATION (MSG#81)
       1 Message::ENABLE_TIME_CONSTRAINED (MSG#83)
       1 Message::GET_OBJECT_CLASS_HANDLE (MSG#113)
       5 Message::GET_ATTRIBUTE_HANDLE (MSG#115)
       1 Message::GET_INTERACTION_CLASS_HANDLE (MSG#117)
       2 Message::GET_PARAMETER_HANDLE (MSG#119)
       5 Message::TICK_REQUEST (MSG#142)
       5 Message::TICK_REQUEST_NEXT (MSG#143)
       1 Message::JOIN_FEDERATION_EXECUTION_V4 (MSG#149)

List of RTI initiated services 
--------------------------------------------------
       1 NetworkMessage::Type::SET_TIME_REGULATING (MSG#8)
       1 NetworkMessage::Type::TIME_REGULATION_ENABLED (MSG#10)
       1 NetworkMessage::Type::TIME_CONSTRAINED_ENABLED (MSG#11)
       1 NetworkMessage::Type::CONFIRM_SYNCHRONIZATION_POINT_REGISTRATION (MSG#13)
       1 NetworkMessage::Type::ANNOUNCE_SYNCHRONIZATION_POINT (MSG#14)
       1 NetworkMessage::Type::FEDERATION_SYNCHRONIZED (MSG#16)

 Number of Federate messages : 26
 Number of RTIG messages : 6
 TCP Socket  3 : total =       437 Bytes sent 
 TCP Socket  3 : total =       771 Bytes received
 UDP Socket  4 : total =         0 Bytes sent 
 UDP Socket  4 : total =         0 Bytes received
```

O erro ocorreu porque:
- Você iniciou a simulação com o nome de um federado que já existia na federação. Provavelmente em uma execução anterior e você ainda não reiniciou o `rtig`. 
- Reinicie o `rtig` e também a simulação.

### RTI::NameNotFound

```bash
~/workspace/certi/build/test/Drones$ ./drones ex1
Created Federation
Joined Federation as ex1
terminate called after throwing an instance of 'RTI::NameNotFound'
RTIA:: RTIA has thrown NetworkError exception.Aborted (core dumped)

RTIA:: Reason: Connection closed by client.

RTIA: Statistics (processed messages)
List of federate initiated services 
--------------------------------------------------
       1 Message::OPEN_CONNEXION (MSG#1)
       1 Message::GET_OBJECT_CLASS_HANDLE (MSG#113)
       5 Message::GET_ATTRIBUTE_HANDLE (MSG#115)
       1 Message::CREATE_FEDERATION_EXECUTION_V4 (MSG#148)
       1 Message::JOIN_FEDERATION_EXECUTION_V4 (MSG#149)

List of RTI initiated services 
--------------------------------------------------

 Number of Federate messages : 9
 Number of RTIG messages : 0
 TCP Socket  3 : total =       209 Bytes sent 
 TCP Socket  3 : total =       375 Bytes received
 UDP Socket  4 : total =         0 Bytes sent 
 UDP Socket  4 : total =         0 Bytes received
```

O erro ocorreu porque:
- Você tentou inicializar uma classe de interação através da chamada do método `getInteractionClassHandle( "X" );`. Este "X" fornecido deve também existir dentro do grupo de objetos "interactions" do arquivo `.fed`.

### RTI::InteractionClassNotDefined

```bash
~/workspace/certi/build/test/Drones$ ./drones ex1
Created Federation
Joined Federation as ex1
Synchronization point announced: ReadyToRun
Successfully registered sync point: ReadyToRun
 >>>>>>>>>> Press Enter to Continue <<<<<<<<<<

Achieved sync point: ReadyToRun, waiting for federation...
Federation Synchronized: ReadyToRun
Time Policy Enabled
 >>>>>>>>>> Atributo criado <<<<<<<<<<
 >>>>>>>>>> Atributos adicionados <<<<<<<<<<
 >>>>>>>>>> Atributo publicado <<<<<<<<<<
 >>>>>>>>>> Atributo subinscrito <<<<<<<<<<
terminate called after throwing an instance of 'RTI::InteractionClassNotDefined'
RTIA:: RTIA has thrown Aborted (core dumped)
NetworkError exception.
RTIA:: Reason: Connection closed by client.

RTIA: Statistics (processed messages)
List of federate initiated services 
--------------------------------------------------
       1 Message::OPEN_CONNEXION (MSG#1)
       1 Message::REGISTER_FEDERATION_SYNCHRONIZATION_POINT (MSG#7)
       1 Message::SYNCHRONIZATION_POINT_ACHIEVED (MSG#11)
       1 Message::PUBLISH_OBJECT_CLASS (MSG#29)
       1 Message::SUBSCRIBE_OBJECT_CLASS_ATTRIBUTES (MSG#33)
       1 Message::ENABLE_TIME_REGULATION (MSG#81)
       1 Message::ENABLE_TIME_CONSTRAINED (MSG#83)
       1 Message::GET_OBJECT_CLASS_HANDLE (MSG#113)
       5 Message::GET_ATTRIBUTE_HANDLE (MSG#115)
       5 Message::TICK_REQUEST (MSG#142)
       5 Message::TICK_REQUEST_NEXT (MSG#143)
       1 Message::CREATE_FEDERATION_EXECUTION_V4 (MSG#148)
       1 Message::JOIN_FEDERATION_EXECUTION_V4 (MSG#149)

List of RTI initiated services 
--------------------------------------------------
       1 NetworkMessage::Type::SET_TIME_REGULATING (MSG#8)
       1 NetworkMessage::Type::TIME_REGULATION_ENABLED (MSG#10)
       1 NetworkMessage::Type::TIME_CONSTRAINED_ENABLED (MSG#11)
       1 NetworkMessage::Type::CONFIRM_SYNCHRONIZATION_POINT_REGISTRATION (MSG#13)
       1 NetworkMessage::Type::ANNOUNCE_SYNCHRONIZATION_POINT (MSG#14)
       1 NetworkMessage::Type::FEDERATION_SYNCHRONIZED (MSG#16)
       2 NetworkMessage::Type::START_REGISTRATION_FOR_OBJECT_CLASS (MSG#90)

 Number of Federate messages : 25
 Number of RTIG messages : 8
 TCP Socket  3 : total =       489 Bytes sent 
 TCP Socket  3 : total =       849 Bytes received
 UDP Socket  4 : total =         0 Bytes sent 
 UDP Socket  4 : total =         0 Bytes receive
```

O erro ocorreu porque:
- Você não inicializou os objetos que tratam da interação (`InteractionClassHandle` e `ParameterHandle`).
- Provavelmente eles já estão declarados no objeto *Federate*, mas não foram inicializados no método `initializeHandles`.
- Por isso o erro ocorreu ao executar o método `publishInteractionClass` que passa como argumento uma variável do tipo `InteractionClassHandle` (vazia porque não foi inicializada).

### RTI::InteractionParameterNotDefined

```bash
valberto@vvc:~/workspace/certi/build/test/Drones$ ./drones ex2
Created Federation
Joined Federation as ex2
Synchronization point announced: ReadyToRun
Successfully registered sync point: ReadyToRun
 >>>>>>>>>> Press Enter to Continue <<<<<<<<<<

Achieved sync point: ReadyToRun, waiting for federation...
Federation Synchronized: ReadyToRun
Time Policy Enabled
 >>>>>>>>>> Atributo criado <<<<<<<<<<
 >>>>>>>>>> Atributos adicionados <<<<<<<<<<
 >>>>>>>>>> Atributo publicado <<<<<<<<<<
 >>>>>>>>>> Atributo subinscrito <<<<<<<<<<
Published and Subscribed
==========================
RESPONSES FROM ROOT OBJECT
==========================
Registered Object, handle=1
>>>>>>>>>> Sending interaction <<<<<<<<<<<<
terminate called after throwing an instance of 'RTI::InteractionParameterNotDefined'
RTIA:: RTIA has thrown NetworkError exception.
RTIA:: Reason: Connection closed by client.Aborted (core dumped)


RTIA: Statistics (processed messages)
List of federate initiated services 
--------------------------------------------------
       1 Message::OPEN_CONNEXION (MSG#1)
       1 Message::REGISTER_FEDERATION_SYNCHRONIZATION_POINT (MSG#7)
       1 Message::SYNCHRONIZATION_POINT_ACHIEVED (MSG#11)
       1 Message::PUBLISH_OBJECT_CLASS (MSG#29)
       1 Message::PUBLISH_INTERACTION_CLASS (MSG#31)
       1 Message::SUBSCRIBE_OBJECT_CLASS_ATTRIBUTES (MSG#33)
       1 Message::SUBSCRIBE_INTERACTION_CLASS (MSG#35)
       1 Message::REGISTER_OBJECT_INSTANCE (MSG#41)
       1 Message::UPDATE_ATTRIBUTE_VALUES (MSG#42)
       1 Message::ENABLE_TIME_REGULATION (MSG#81)
       1 Message::ENABLE_TIME_CONSTRAINED (MSG#83)
       2 Message::GET_OBJECT_CLASS_HANDLE (MSG#113)
       5 Message::GET_ATTRIBUTE_HANDLE (MSG#115)
       1 Message::GET_INTERACTION_CLASS_HANDLE (MSG#117)
       2 Message::GET_PARAMETER_HANDLE (MSG#119)
      10 Message::TICK_REQUEST (MSG#142)
       5 Message::TICK_REQUEST_NEXT (MSG#143)
       1 Message::CREATE_FEDERATION_EXECUTION_V4 (MSG#148)
       1 Message::JOIN_FEDERATION_EXECUTION_V4 (MSG#149)

List of RTI initiated services 
--------------------------------------------------
       1 NetworkMessage::Type::ADDITIONAL_FOM_MODULE (MSG#6)
       2 NetworkMessage::Type::SET_TIME_REGULATING (MSG#8)
       1 NetworkMessage::Type::TIME_REGULATION_ENABLED (MSG#10)
       1 NetworkMessage::Type::TIME_CONSTRAINED_ENABLED (MSG#11)
       1 NetworkMessage::Type::CONFIRM_SYNCHRONIZATION_POINT_REGISTRATION (MSG#13)
       1 NetworkMessage::Type::ANNOUNCE_SYNCHRONIZATION_POINT (MSG#14)
       1 NetworkMessage::Type::FEDERATION_SYNCHRONIZED (MSG#16)
       1 NetworkMessage::Type::DISCOVER_OBJECT (MSG#44)
       1 NetworkMessage::Type::REFLECT_ATTRIBUTE_VALUES (MSG#46)
       4 NetworkMessage::Type::START_REGISTRATION_FOR_OBJECT_CLASS (MSG#90)

 Number of Federate messages : 38
 Number of RTIG messages : 14
 TCP Socket  3 : total =       710 Bytes sent 
 TCP Socket  3 : total =      1625 Bytes received
 UDP Socket  5 : total =         0 Bytes sent 
 UDP Socket  5 : total =         0 Bytes received
```

O erro ocorreu porque:
 - Um ou mais parâmetros de interação não foram inicializados.
 - No método `initializeHandles()`, verifique se os valores dos parâmetros a serem transmitidos/recebidos na interação estão corretamente inicializados.


## Built With

.

## Contributing

Please read CONTRIBUTING.md for details on our code of conduct, and the process for submitting pull requests to us.

## Authors

* **Alisson Brito** - *Professor*
* **Phillip Paiva** - *Student*
* **Valberto Carneiro** - *Student*

## License

.
