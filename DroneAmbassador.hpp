#ifndef DroneAmbassador_H_
#define DroneAmbassador_H_

#include <map>
#include <memory>
#include <RTI.hh>
#include <NullFederateAmbassador.hh>
#include "Drone.hpp"

using namespace std;

class DroneAmbassador : public NullFederateAmbassador {
public:
  // variables //
  double federateTime;
  double federateLookahead;

  bool isRegulating;
  bool isConstrained;
  bool isAdvancing;
  bool isAnnounced;
  bool isReadyToRun;

  string federateName;
  Drone* local;
  map<RTI::ObjectHandle, Drone> remote;
  bool hasData;
  bool hasColision;

  // methods //
  DroneAmbassador(const string &federateName, Drone &local);
  virtual ~DroneAmbassador() throw( RTI::FederateInternalError );

  void calculateDistance();

  ///////////////////////////////////
  // synchronization point methods //
  ///////////////////////////////////
  virtual void synchronizationPointRegistrationSucceeded(const char *label)
  throw(RTI::FederateInternalError);

  virtual void synchronizationPointRegistrationFailed(const char *label)
  throw(RTI::FederateInternalError);

  virtual void announceSynchronizationPoint(const char *label, const char *tag)
  throw(RTI::FederateInternalError);

  virtual void federationSynchronized(const char *label)
  throw(RTI::FederateInternalError);

  //////////////////////////
  // time related methods //
  //////////////////////////
  virtual void timeRegulationEnabled(const RTI::FedTime &theFederateTime)
  throw(RTI::InvalidFederationTime,
  RTI::EnableTimeRegulationWasNotPending,
  RTI::FederateInternalError);

  virtual void timeConstrainedEnabled(const RTI::FedTime &theFederateTime)
  throw(RTI::InvalidFederationTime,
  RTI::EnableTimeConstrainedWasNotPending,
  RTI::FederateInternalError);

  virtual void timeAdvanceGrant(const RTI::FedTime &theTime)
  throw(RTI::InvalidFederationTime,
  RTI::TimeAdvanceWasNotInProgress,
  RTI::FederateInternalError);

  ///////////////////////////////
  // object management methods //
  ///////////////////////////////
  virtual void discoverObjectInstance(
      RTI::ObjectHandle theObject,
      RTI::ObjectClassHandle theObjectClass,
      const char *theObjectName)
  throw(RTI::CouldNotDiscover,
  RTI::ObjectClassNotKnown,
  RTI::FederateInternalError);

  virtual void reflectAttributeValues(
      RTI::ObjectHandle theObject,
      const RTI::AttributeHandleValuePairSet &theAttributes,
      const RTI::FedTime &theTime,
      const char *theTag,
      RTI::EventRetractionHandle theHandle)
  throw(RTI::ObjectNotKnown,
  RTI::AttributeNotKnown,
  RTI::FederateOwnsAttributes,
  RTI::InvalidFederationTime,
  RTI::FederateInternalError);

  virtual void receiveInteraction(
      RTI::InteractionClassHandle theInteraction,
      const RTI::ParameterHandleValuePairSet &theParameters,
      const RTI::FedTime &theTime,
      const char *theTag,
      RTI::EventRetractionHandle theHandle)
  throw(RTI::InteractionClassNotKnown,
  RTI::InteractionParameterNotKnown,
  RTI::InvalidFederationTime,
  RTI::FederateInternalError);

  virtual void removeObjectInstance(
      RTI::ObjectHandle theObject,
      const RTI::FedTime &theTime,
      const char *theTag,
      RTI::EventRetractionHandle theHandle)
  throw(RTI::ObjectNotKnown,
  RTI::InvalidFederationTime,
  RTI::FederateInternalError);

  virtual void removeObjectInstance(RTI::ObjectHandle theObject, const char *theTag)
  throw(RTI::ObjectNotKnown, RTI::FederateInternalError);

  virtual void turnUpdatesOnForObjectInstance(RTI::ObjectHandle, const RTI::AttributeHandleSet &)
  throw (RTI::ObjectNotKnown, RTI::AttributeNotOwned, RTI::FederateInternalError);

  /////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////// Private Section ////////////////////////////////////
  /////////////////////////////////////////////////////////////////////////////////////////
private:
  double convertTime(const RTI::FedTime &theTime);

};

#endif /*DroneAmbassador_H_*/
