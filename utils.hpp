#ifndef UTILS_HPP
#define UTILS_HPP

#include <iostream>
#include <string>
#include <vector>

#include "RTI.hh"

#define radians(angleDegrees) ((angleDegrees) * M_PI / 180.0)
#define degrees(angleRadians) ((angleRadians) * 180.0 / M_PI)

int randint(int start = 0, int stop = 10);


void split(std::string const &str, const char delim, std::vector<std::string> &out);

#endif // UTILS_HPP