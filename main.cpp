#include <iostream>
#include <omp.h>
#include <string.h>
#include "DroneFederate.hpp"

#define NUMBER_OF_DRONES 10
#define STEPS 10
#define FED_FILE_NAME "drone.fed"
#define PROXIMITY_LIMIT 50

using namespace std;

int main() {
  DroneFederate *federates[NUMBER_OF_DRONES];
  string federateName;
  for (unsigned int i=0; i < NUMBER_OF_DRONES; i++) {
    federateName = "drone-" + to_string(i);
    federates[i] = new DroneFederate(federateName, "federation", FED_FILE_NAME, PROXIMITY_LIMIT);
  }
  #pragma omp parallel for
  for (unsigned int i=0; i < STEPS; i++) {
    for (unsigned int j=0; j < NUMBER_OF_DRONES; j++) {
      federates[j]->move();
    }
  }
  cout << "Simulation end. Press enter" << endl;
  cin.ignore();
  for (unsigned int i=0; i < NUMBER_OF_DRONES; i++) {
    delete federates[i];
  }
  return 0;
}
