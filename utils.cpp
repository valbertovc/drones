#include <random>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "RTI.hh"

int randint(int start = 0, int stop = 10) {
  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<std::mt19937::result_type> dist_value(start, stop);
  return dist_value(rng);
}

void
split(std::string const &str, const char delim, std::vector<std::string> &out)
{
  size_t start;
  size_t end = 0;

  while ((start = str.find_first_not_of(delim, end)) != std::string::npos)
  {
    end = str.find(delim, start);
    out.push_back(str.substr(start, end - start));
  }
}
