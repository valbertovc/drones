#ifndef DRONE_HPP
#define DRONE_HPP

#include "RTI.hh"

using namespace std;

class Drone {
public:
  Drone(RTI::ObjectHandle);

  RTI::ObjectHandle handle;
  unsigned long id;
  unsigned int proximity_limit;

  double getPosX();
  double getPosY();
  int getSpeed();
  int getTeta();
  void setPosX(double posX);
  void setPosY(double posY);
  void setSpeed(int speed);
  void setTeta(int teta);
  double distance(Drone other);
  void move(int time);
private:
  double posX;
  double posY;
  int speed;
  int teta;
};

#endif //DRONE_HPP