#include <iostream>
#include <string>
#include "RTI.hh"
#include "fedtime.hh"
#include "DroneAmbassador.hpp"
#include "DroneFederate.hpp"
#include "utils.hpp"

using namespace std;

class DroneAmbassador;

DroneFederate::DroneFederate(
    const string federateName,
    const string federationName,
    const string fedFileName,
    const unsigned int proximity) {
  this->drone = new Drone(0);
  this->drone->proximity_limit = proximity;
  this->rtiamb = new RTI::RTIambassador();
  this->ambassador = new DroneAmbassador(federateName, *this->drone);
  this->federateName = federateName;
  this->federationName = federationName;
  this->fedFileName = fedFileName;
  joinFederation();
  initializeHandles();
  publishAndSubscribe();
  registerObject();
  cout << "Drone " << this->drone->id << " created." << endl;
}


/* Destroy the drone federate and yours dependents. */
DroneFederate::~DroneFederate() {
  rtiamb->deleteObjectInstance(this->drone->id, nullptr);
  rtiamb->resignFederationExecution(RTI::NO_ACTION);
  delete ambassador;
}


/* Join the drone federate to federation */
RTI::FederateHandle DroneFederate::joinFederation() {
  try {
    rtiamb->createFederationExecution(federationName.c_str(), fedFileName.c_str());
    cout << "Created Federation" << endl;
  }
  catch (RTI::FederationExecutionAlreadyExists &exists) {
    cout << "Federation \"" << federationName << "\" already existed" << endl;
  }
  return rtiamb->joinFederationExecution(
      federateName.c_str(),
      federationName.c_str(),
      ambassador);
}


/* Stop execution from federation */
void DroneFederate::stopFederation(){
  try {
    rtiamb->destroyFederationExecution(federationName.c_str());
    cout << "Destroyed Federation" << endl;
  }
  catch (RTI::FederationExecutionDoesNotExist &dne) {
    cout << "No need to destroy federation, it doesn't exist" << endl;
  }
  catch (RTI::FederatesCurrentlyJoined &fcj) {
    cout << "Didn't destroy federation, federates still joined" << endl;
  }

  //////////////////
  // 13. clean up //
  //////////////////
  delete this->rtiamb;
}


/* Moves the drone x times at the current speed */
void DroneFederate::move() {
  if (ambassador->hasColision) {
    unsigned int new_teta = randint(0, 360);
    this->drone->setTeta(new_teta);
    ambassador->hasColision = false;
    cout << "Drone " << this->drone->id
         << ": Colision imminent! New direction defined to: " << new_teta << endl;
  }
  this->drone->move(1);
  cout << "Drone " << this->drone->id << ": moved." << endl;
  updateAttributeValues(this->drone->id);
  if (ambassador->hasData) {
    ambassador->calculateDistance();
    ambassador->hasData = false;
  }

  advanceTime(1.0);
  cout << "Drone " << this->drone->id << ": time advanced to " << ambassador->federateTime << endl;
}


///////////////////////////////////////////////////////////////////////////////
//////////////////////////////// Helper Methods ///////////////////////////////
///////////////////////////////////////////////////////////////////////////////
/*
 * This method will get all the relevant handle information from the RTIambassador
 */
void DroneFederate::initializeHandles() {
  this->droneClassHandle = rtiamb->getObjectClassHandle("ObjectRoot.drone");
  this->idHandle = rtiamb->getAttributeHandle("id", droneClassHandle);
  this->posXHandle = rtiamb->getAttributeHandle("posX", droneClassHandle);
  this->posYHandle = rtiamb->getAttributeHandle("posY", droneClassHandle);
  this->speedHandle = rtiamb->getAttributeHandle("speed", droneClassHandle);
  this->tetaHandle = rtiamb->getAttributeHandle("teta", droneClassHandle);
  this->iHandle = rtiamb->getInteractionClassHandle("InteractionRoot.Position");
  this->xHandle = rtiamb->getParameterHandle("x", this->iHandle);
  this->yHandle = rtiamb->getParameterHandle("y", this->iHandle);
  this->idHandle = rtiamb->getParameterHandle("id", this->iHandle);
}

/*
 * This method will inform the RTI about the types of data that the federate will
 * be creating, and the types of data we are interested in hearing about as other
 * federates produce it.
 */
void DroneFederate::publishAndSubscribe() {
  ////////////////////////////////////////////
  // publish all attributes of ObjectRoot.drone //
  ////////////////////////////////////////////
  // before we can register instance of the object class ObjectRoot.drone and
  // update the values of the various attributes, we need to tell the RTI
  // that we intend to publish this information

  // package the information into a handle set
  RTI::AttributeHandleSet *attributes = RTI::AttributeHandleSetFactory::create(5);
  cout << " >>>>>>>>>> Atributo criado <<<<<<<<<<" << endl;

  attributes->add(this->idHandle);
  attributes->add(this->posXHandle);
  attributes->add(this->posYHandle);
  attributes->add(this->speedHandle);
  attributes->add(this->tetaHandle);

  cout << " >>>>>>>>>> Atributos adicionados <<<<<<<<<<" << endl;

  // do the actual publication
  rtiamb->publishObjectClass(this->droneClassHandle, *attributes);

  cout << " >>>>>>>>>> Atributo publicado <<<<<<<<<<" << endl;


  /////////////////////////////////////////////////
  // subscribe to all attributes of ObjectRoot.drone //
  /////////////////////////////////////////////////
  // we also want to hear about the same sort of information as it is
  // created and altered in other federates, so we need to subscribe to it
  rtiamb->subscribeObjectClassAttributes(this->droneClassHandle, *attributes);
  cout << " >>>>>>>>>> Atributo subinscrito <<<<<<<<<<" << endl;


  /////////////////////////////////////////////////////
  // publish the interaction class InteractionRoot.X //
  /////////////////////////////////////////////////////
  // we want to send interactions of type InteractionRoot.X, so we need
  // to tell the RTI that we're publishing it first. We don't need to
  // inform it of the parameters, only the class, making it much simpler

  // do the publication
  rtiamb->publishInteractionClass(this->iHandle);

  ////////////////////////////////////////////////////
  // subscribe to the InteractionRoot.X interaction //
  ////////////////////////////////////////////////////
  // we also want to receive other interaction of the same type that are
  // sent out by other federates, so we have to subscribe to it first
  rtiamb->subscribeInteractionClass(this->iHandle);

  // clean up
  delete attributes;
}

/*
 * This method will register an instance of the class ObjectRoot.A and will
 * return the federation-wide unique handle for that instance. Later in the
 * simulation, we will update the attribute values for this instance
 */
void DroneFederate::registerObject() {
  this->drone->id = rtiamb->registerObjectInstance(
      rtiamb->getObjectClassHandle("ObjectRoot.drone"));
  cout << "Drone " << this->drone->id << ": registered" << endl;
  RTI::AttributeHandleSet *attributes = RTI::AttributeHandleSetFactory::create(5);
  attributes->add(this->idHandle);
  attributes->add(this->posXHandle);
  attributes->add(this->posYHandle);
  attributes->add(this->speedHandle);
  attributes->add(this->tetaHandle);
  this->ambassador->turnUpdatesOnForObjectInstance(this->drone->id, *attributes);
}

RTI::AttributeHandleValuePairSet* DroneFederate::attributes() {

  RTI::AttributeHandleValuePairSet *attributes = RTI::AttributeSetFactory::create(5);

  // generate the new values
  // we use EncodingHelpers to make things nice friendly for both Java and C++
  char idValue[16], posXValue[16], posYValue[16], speedValue[16], tetaValue[16];

  sprintf(idValue, "id:%s", to_string(this->drone->id).c_str());
  sprintf(posXValue, "posX:%f", this->drone->getPosX());
  sprintf(posYValue, "posY:%f", this->drone->getPosY());
  sprintf(speedValue, "speed:%d", this->drone->getSpeed());
  sprintf(tetaValue, "teta:%d", this->drone->getTeta());

  attributes->add(idHandle, idValue, (RTI::ULong) strlen(idValue) + 1);
  attributes->add(posXHandle, posXValue, (RTI::ULong) strlen(posXValue) + 1);
  attributes->add(posYHandle, posYValue, (RTI::ULong) strlen(posYValue) + 1);
  attributes->add(speedHandle, speedValue, (RTI::ULong) strlen(speedValue) + 1);
  attributes->add(tetaHandle, tetaValue, (RTI::ULong) strlen(tetaValue) + 1);
  return attributes;
}
/*
 * This method will update all the values of the given object instance. It will
 * set each of the values to be a string which is equal to the name of the
 * attribute plus the current time. eg "aa:10.0" if the time is 10.0.
 * <p/>
 * Note that we don't actually have to update all the attributes at once, we
 * could update them individually, in groups or not at all!
 */
void DroneFederate::updateAttributeValues(RTI::ObjectHandle object) {
  RTI::AttributeHandleValuePairSet *attributes = this->attributes();
  RTIfedTime time = ambassador->federateTime + ambassador->federateLookahead;
  rtiamb->updateAttributeValues(object, *attributes, time, to_string(this->drone->id).c_str());
  delete attributes;
}

/*
 * This method will send out an interaction of the type InteractionRoot.X. Any
 * federates which are subscribed to it will receive a notification the next time
 * they tick(). Here we are passing only two of the three parameters we could be
 * passing, but we don't actually have to pass any at all!
 */
void DroneFederate::sendInteraction() {
  ///////////////////////////////////////////////
  // create the necessary container and values //
  ///////////////////////////////////////////////
  // create the collection to store the values in
  RTI::ParameterHandleValuePairSet *parameters = RTI::ParameterSetFactory::create(3);

  // generate the new values
  char xValue[16], yValue[16], idValue[16];
  sprintf(xValue, "x:%f", this->drone->getPosX());
  sprintf(yValue, "y:%f", this->drone->getPosY());
  sprintf(idValue, "id:%s", to_string(this->drone->id).c_str());
  parameters->add(xHandle, xValue, (RTI::ULong) strlen(xValue) + 1);
  parameters->add(yHandle, yValue, (RTI::ULong) strlen(yValue) + 1);
  parameters->add(idHandle, idValue, (RTI::ULong) strlen(idValue) + 1);

  //////////////////////////
  // send the interaction //
  //////////////////////////
  cout << "Drone " << this->drone->id << ": Sending interaction" << endl;
  // rtiamb->sendInteraction( iHandle, *parameters, "hi!" );

  // if you want to associate a particular timestamp with the
  // interaction, you will have to supply it to the RTI. Here
  // we send another interaction, this time with a timestamp:
  RTIfedTime time = ambassador->federateTime + ambassador->federateLookahead;
  string tag = "Hi! I am the drone " + to_string(this->drone->id);
  rtiamb->sendInteraction(iHandle, *parameters, time, tag.c_str());

  // clean up
  delete parameters;
}

/*
 * This method will request a time advance to the current time, plus the given
 * timestep. It will then wait until a notification of the time advance grant
 * has been received.
 */
void DroneFederate::advanceTime(double timestep) {

  // request the advance
  ambassador->isAdvancing = true;
  RTIfedTime newTime = (ambassador->federateTime + timestep);
  rtiamb->timeAdvanceRequest(newTime);

  // wait for the time advance to be granted. ticking will tell the
  // LRC to start delivering callbacks to the federate
  while (ambassador->isAdvancing) {
    rtiamb->tick();
  }
}

double DroneFederate::getLbts() {
  return (ambassador->federateTime + ambassador->federateLookahead);
}
