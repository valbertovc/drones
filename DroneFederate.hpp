#ifndef DRONEFEDERATE_H_
#define DRONEFEDERATE_H_

#include "RTI.hh"
#include "DroneAmbassador.hpp"
#include "Drone.hpp"

class DroneFederate {
public:
  Drone *drone;
  // receve data and interactions from RTI
  RTI::RTIambassador *rtiamb;
  // send data and interactions to RTI
  DroneAmbassador *ambassador;
  // federation name that will be joined
  string federateName;
  // FOM file name with objects and interaction specifications
  string federationName;
  // FOM file name with objects and interaction specifications
  string fedFileName;
  // FOM handles //
  RTI::ObjectClassHandle droneClassHandle;
  RTI::AttributeHandle idHandle;
  RTI::AttributeHandle posXHandle;
  RTI::AttributeHandle posYHandle;
  RTI::AttributeHandle speedHandle;
  RTI::AttributeHandle tetaHandle;
  // interaction
  RTI::InteractionClassHandle iHandle;
  RTI::ParameterHandle xHandle;
  RTI::ParameterHandle yHandle;

  // constructors
  DroneFederate(
      const std::string federateName,
      const std::string federationName,
      const std::string fedFileName,
      const unsigned int proximity);

  // destructor
  virtual ~DroneFederate();

  // make drone movement
  void move();
  void stopFederation();
  RTI::FederateHandle joinFederation();
  void registerObject();

private:
  void initializeHandles();
  void publishAndSubscribe();
  RTI::AttributeHandleValuePairSet* attributes();
  void updateAttributeValues(RTI::ObjectHandle objectHandle);
  void sendInteraction();
  void advanceTime(double timestep);
  double getLbts();
};

#endif /*EXAMPLECPPFEDERATE_H_*/
